import json
import traceback
from flask import (
    Flask,
    request,
    jsonify,
    current_app as app,
    Blueprint,
)
from functools import wraps

from project import create_app

main = Blueprint("main", __name__)

# Catch all error handler
@main.errorhandler(500)
def handle_error(e):
    # logger.error(e)
    # logger.exception("Uncaught application at application.py level")
    return make_error_message("Unexpected server error, please try again")


# Error response support function
def make_error_message(message):
    return jsonify({"error": message}), 400


# Add authentication wrapper
def auth_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth_header = request.headers.get("Authorization")
        try:
            # Parses out the "Bearer" portion
            token = auth_header.split(" ")[1]
            if not token:
                return (
                    jsonify(
                        {
                            "message": "Token is missing. Please login.",
                            "status": "faliure",
                        }
                    ),
                    401,
                )

            decoded = verifier.decodeJWTAuthToken(token)
            if "expired" in decoded:
                return (
                    jsonify(
                        {
                            "message": "Token is expired. Please login.",
                            "status": "faliure",
                        }
                    ),
                    401,
                )
            if "invalid" in decoded:
                return (
                    jsonify(
                        {
                            "message": "Token is invalid. Please login.",
                            "status": "faliure",
                        }
                    ),
                    401,
                )
        except IndexError as e:
            print("Error", str(e))
            return (
                jsonify(
                    {"message": "Token is missing. Please login.", "status": "faliure"}
                ),
                401,
            )
        except Exception as e:
            print("Error", str(e))
            return (
                jsonify(
                    {"message": "Token is invalid. Please login.", "status": "faliure"}
                ),
                401,
            )
        return f(*args, **kwargs)

    return decorated


# Endpoints
@main.route("ping", methods=["POST", "GET"])
def ping():
    return jsonify({"status": "pong"})


@main.route("try_traceback", methods=["GET"])
def try_traceback():
    """ An exmaple for tracing original handled error by try-except with traceback package. """
    try:
        raise Exception("DANGER!")
    except:
        app.logger.error(
            f"This is an error Message at: \n\n {traceback.format_exc()}")
    return jsonify({"status": "Works"})