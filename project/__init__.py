import os
import sys
import json
import time
import logging

from flask_cors import CORS
from flask import Flask
from project.logger import CustomLogger
from config import configs

# flask extensions

# custom logger
custom_logger = CustomLogger()

# import models to let Flask_Sqlalchemy know


def create_app(main=True):
    # initialise Flask
    app = Flask(__name__)

    # load app configs
    if app.debug:
        config_name = "development"
    elif app.testing:
        config_name = "testing"
    else:
        config_name = "production"
    app.config.from_object(configs[config_name])

    # set the CORS settings (No Cors domain restrictions by default)
    allowed_origins = "*"
    cors_headers = [
        "Access-Control-Allow-Credentials",
        "Access-Control-Allow-Origin",
        "Content-Type",
        "Accept",
    ]
    CORS(app, origins=allowed_origins,
         supports_credentials=True, headers=cors_headers)

    # initialize custom logger
    with app.app_context():
        custom_logger.init_app(level=logging.INFO, mail=True)

        app.logger.error(f"Flask-App Application Launched: config_name {config_name}")

        # import routes blueprint
        from project.routes import main as main_blueprint

        app.register_blueprint(main_blueprint, url_prefix="/api")

    return app
