# api/config.py

import os
from dotenv import load_dotenv, find_dotenv

basedir = os.path.abspath(os.path.dirname(__file__))

if os.getenv("FLASK_ENV", None).lower() == "production":
    filename = ".env"
elif os.getenv("FLASK_ENV", None).lower() == "development" and os.getenv("LOCAL_DOCKER", None):
    filename = ".env.development"
else:
    filename = ".env.development.local"


load_dotenv(
    find_dotenv(filename=filename, raise_error_if_not_found=True),
    override=True,
)

print("base_dir", basedir)
print("env_file", filename )


class BaseConfig(object):
    """Base configuration."""

    SECRET_KEY = os.getenv("SECRET_KEY", "4dca5dfb-1acd-4353-8a7d-89d4f025225c")
    DEBUG = False
    TESTING = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_DATABASE_URI = os.getenv("DATABASE_URL", "sqlite:///temp.db")
    # SQLALCHEMY_ENGINE_OPTIONS = {'pool_size' : 100, 'pool_recycle' : 3600, 'pool_pre_ping': True}
    SQLALCHEMY_ENGINE_OPTIONS = {"pool_pre_ping": True}

    """Logger"""
    LOG_TO_STDOUT = False
    PROD_LOG_MAIL = False

    """Mail Config For Email Handler"""
    # MAIL_PORT = int(os.getenv('MAIL_PORT') or 587)
    # MAIL_USE_TLS = os.getenv('MAIL_USE_TLS', False)
    MAIL_SERVER = os.getenv("MAIL_SERVER", "smtp.gmail.com")
    MAIL_PORT = int(os.getenv("MAIL_PORT") or 465)
    MAIL_USE_SSL = os.getenv("MAIL_USE_TLS", True)
    MAIL_USERNAME = os.getenv("MAIL_USERNAME", "sarthak@coreintelligence.com.au")
    MAIL_DEFAULT_SENDER = os.getenv(
        "MAIL_DEFAULT_SENDER", "sarthak@coreintelligence.com.au"
    )
    MAIL_PASSWORD = os.getenv("MAIL_PASSWORD", "CoreaiID977{")
    ADMIN_EMAIL = os.getenv("ADMIN_EMAIL", "sarthak@coreintelligence.com.au")

    """File Uploads Config"""
    IMAGE_UPLOADS = f"{basedir}/project/static/logos/"
    CLIENT_CSV_UPLOADS = f"{basedir}/project/static/client_csv/"
    ALLOWED_IMAGE_EXTENSIONS = ["JPEG", "JPG", "PNG", "GIF"]
    ALLOWED_FILE_EXTENSIONS = ["PDF", "DOCX", "DOC", "TXT", "CSV"]


class DevelopmentConfig(BaseConfig):
    """Development configuration."""

    DEBUG = True
    LOG_TO_STDOUT = True
    SQLALCHEMY_DATABASE_URI = os.getenv("DATABASE_DEV_URL")


class TestingConfig(BaseConfig):
    """Testing configuration."""

    TESTING = True
    LOG_TO_STDOUT = True
    PRESERVE_CONTEXT_ON_EXCEPTION = False
    # SQLALCHEMY_DATABASE_URI = os.getenv("DATABASE_TEST_URL")
    SOCKETIO_MESSAGE_QUEUE = None


class ProductionConfig(BaseConfig):
    """Production configuration."""
    DEBUG = False
    PROD_LOG_MAIL = True
    SQLALCHEMY_DATABASE_URI = os.getenv("DATABASE_URL")


configs = {
    "development": DevelopmentConfig,
    "testing": TestingConfig,
    "production": ProductionConfig,
    "default": BaseConfig,
}

error_handler_config = {
    "version": 1,
    "formatters": {
        "default": {
            "format": "[%(asctime)s] %(levelname)s: %(message)s "
            "[in %(pathname)s:%(lineno)d]",
        }
    },
    "handlers": {
        "wsgi": {
            "class": "logging.StreamHandler",
            "stream": "ext://flask.logging.wsgi_errors_stream",
            "formatter": "default",
        }
    },
    "root": {"level": "ERROR", "handlers": ["wsgi"]},
}
