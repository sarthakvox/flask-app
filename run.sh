# For Production
# `--reload` will enable debug mode in Prod.
# `--log-file=-`` will create the log file
# `--error-logfile gunicorn.error.log` will specify error log file
# `--access-logfile gunicorn.log` will specify access/requests log file
# `--capture-output` will show the output in console
# gunicorn --worker-class eventlet -w 1 -b 0.0.0.0:5000 application:app --reload --log-file=- --error-logfile gunicorn.error.log --capture-output
export FLASK_ENV=Production

echo $FLASK_ENV

# gunicorn -w 1 -b 0.0.0.0:5000 --preload app:app --reload --log-file gunicorn.log --error-logfile gunicorn.error.log --access-logfile gunicorn.access.log --capture-output