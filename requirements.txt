flask==1.0.2
flask-cors==3.0.8
gunicorn==20.0.4
python-dotenv==0.14.0